# Grow Your Own - Calendar #

An open source project for generating `.ics` files that can be imported to your calendar and show a schedule of the best time to plant and harvest fruit & veg.

![Example calendar](/example.png)

## Adding/Subscribing To A Calendar ##

Calendars are publicly available to subscribe to via GitLab pages.

The current calendars available to subscribe/import are:

* `https://microvist.gitlab.io/grow-calendar/gb.ics`

For instructions on how to subscribe to calendars see:

* [Subscribe to calendars on Mac](https://support.apple.com/en-gb/guide/calendar/icl1022/mac)
* [Subscribe to calendars with Outlook](https://support.microsoft.com/en-us/office/import-or-subscribe-to-a-calendar-in-outlook-com-cff1429c-5af6-41ec-a5b4-74f2c278e98c)
* [Subscribe to calendars with Google](https://support.google.com/calendar/answer/37100?co=GENIE.Platform%3DDesktop&hl=en#zippy=%2Cadd-using-a-link)

## Contributing ##

See out [contributing guidelines](/CONTRIBUTING.md) if you want to help us out in improving this project.

## Shameless Plug ##

Created with love thanks to [microvist.com](https://microvist.com) :heart:
