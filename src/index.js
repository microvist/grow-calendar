const { readdirSync, readFileSync, mkdirSync, writeFileSync } = require('fs')
const path = require('path')
const yaml = require('js-yaml')
const ics = require('ics')

readdirSync('./data/').forEach(file => {
  const region = file.split('.').shift()
  const doc = yaml.load(readFileSync(path.resolve(__dirname, '../data/', file), 'utf8'))
  const events = []

  const calName = doc.calendar.name
  doc.produce.forEach(i => {
    const title = i.name
    i.events.forEach(e => {
      const { description } = e
      const start = e.start.split('/').map(i => parseInt(i))
      const end = e.end.split('/').map(i => parseInt(i))

      events.push({ calName, title, description, start, end })
    })
  })

  const { error, value } = ics.createEvents(events)
  if (error) {
    console.log(error)
    return
  }

  mkdirSync(path.resolve(__dirname, '../public/'), { recursive: true })
  writeFileSync(path.resolve(__dirname, '../public/', `${region}.ics`), value)
  console.log('Done 🎉')
})
