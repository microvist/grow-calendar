# Contributing

To report an issue, add or suggest an improvement feel free to:

* Create an issue in https://gitlab.com/microvist/grow-calendar/-/issues
* Open a pull/merge request
* Drop us an email at `hi@microvist.com`

## Data ##

Data is stored in the `data` directory named following the naming pattern `<country_code>.yml` so that different regions can have their own calendars that suit their own growing conditions.

Each `.yml` file in `data` maps to a `.ics` calendar. For example `data/gb.yml` will have its calendar available at `https://microvist.gitlab.io/grow-calendar/gb.yml`.

The YAML files are structured to record:

* The produce items name (used as the calendar event title for each event)
* A list of events for the item (e.g. when to sow and when to harvest)
* For each event a start date, end date and description

```
produce:
  - name: "Beans – broad"
    events:
      - start: "2021/1/1"
        end: "2021/4/1"
        description: "Sow outside (direct into prepared garden soil)"
      - start: "2021/6/1"
        end: "2021/9/1"
        description: "Harvest (when ready)"
```

Please feel free to contribute the addition or more regions, more produce for a region or updates/corrections to any existing data.

## Generating Calendars To Import ##

For any data updates committed back to the project new `.ics` files are generated and most calendars subscribed to one of these will update themselves to reflect the changes.

The scripts that do this can be found in `src`, constituting of some basic NodeJS scripts and GitLab pipelines to automate updates.
